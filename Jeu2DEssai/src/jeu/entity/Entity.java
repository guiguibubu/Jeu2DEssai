package jeu.entity;

import java.awt.Graphics;
import java.awt.Rectangle;

import jeu.Handler;

public abstract class Entity {
	
	public static final int DEFAULT_HEALTH = 3;
	
	protected Handler handler;
	protected float x, y;
	protected int width, height;
	protected Rectangle bounds;
	
	protected int health;
	
	protected boolean active = true;
	
	public Entity(Handler handler, float x, float y, int width, int height){
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.handler = handler;
		
		this.health = Entity.DEFAULT_HEALTH;
		
		this.bounds = new Rectangle(0, 0, this.width, this.height);
	}
	

	public abstract void update();
	
	public abstract void render(Graphics g);

	public abstract void die();
	
	public void hurt(int amount){
		this.health -= amount;
		if(this.health <= 0){
			this.active = false;
			this.die();
		}
	}
	
	public boolean checkEntityCollision(float xOffset, float yOffset){
		for(Entity e : this.handler.getWorld().getEntityManager().getEntities()){
			if(e.equals(this))
				continue;
			if(e.getCollisionBounds(0f, 0f).intersects(this.getCollisionBounds(xOffset, yOffset))){
				return true;
			}
		}
		return false;
	}
	
	public Rectangle getCollisionBounds(float xOffset, float yOffset){
		return new Rectangle((int) (this.x + this.bounds.x + xOffset), (int) (this.y + this.bounds.y + yOffset), this.bounds.width, this.bounds.height);
	}
	
	public float getX() {
		return this.x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return this.y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public int getWidth() {
		return this.width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return this.height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	
	public int getHealth() {
		return this.health;
	}

	public void setHealth(int health) {
		this.health = health;
	}
	
	public boolean isActive() {
		return this.active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
