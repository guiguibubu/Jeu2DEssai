package jeu.entity.statics;

import java.awt.Graphics;

import jeu.Handler;
import jeu.gfx.Assets;
import jeu.items.Item;
import jeu.tile.Tile;

public class Tree extends StaticEntity {

	public Tree(Handler handler, float x, float y) {
		super(handler, x, y, Tile.TILEWIDTH, Tile.TILEHEIGHT*2);
		this.bounds.y += this.bounds.height/2;
		this.bounds.height/=2;
	}
	

	@Override
	public void update() {

	}

	@Override
	public void render(Graphics g) {
		g.drawImage(Assets.tree, (int) (this.x - this.handler.getGameCamera().getxOffset()), (int) (this.y - this.handler.getGameCamera().getyOffset()), this.width, this.height, null);
	}


	@Override
	public void die() {
		this.handler.getWorld().getItemManager().addItem(Item.woodItem.createNew((int) (this.x + this.bounds.width/2), (int) (this.y + this.bounds.height/2)));	
	}
}
