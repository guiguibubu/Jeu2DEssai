package jeu.entity.statics;

import java.awt.Graphics;

import jeu.Handler;
import jeu.entity.Entity;

public abstract class StaticEntity extends Entity {

	public StaticEntity(Handler handler, float x, float y, int width, int height) {
		super(handler, x, y, width, height);
	}

	@Override
	public void update() {

	}

	@Override
	public void render(Graphics g) {

	}

}
