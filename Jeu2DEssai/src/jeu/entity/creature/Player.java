package jeu.entity.creature;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import jeu.Handler;
import jeu.entity.Entity;
import jeu.gfx.Animation;
import jeu.gfx.Assets;
import jeu.inventory.Inventory;

public class Player extends Creature {
	
	private Animation animDown, animUp, animLeft, animRight;
	
	private long lastAttackTimer, attackCooldown, attackTimer;
	
	private BufferedImage lastImage;
	
	private Inventory inventory;
	
	public Player(Handler handler, float x, float y) {
		super(handler, x, y, Creature.DEFAULT_CREATURE_WIDTH, Creature.DEFAULT_CREATURE_HEIGHT);
		this.bounds.x = 22;
		this.bounds.y = 30;
		this.bounds.width = 20;
		this.bounds.height = 34;
		
		this.animDown = new Animation(500, Assets.player_down);
		this.animUp = new Animation(500, Assets.player_up);
		this.animLeft = new Animation(500, Assets.player_left);
		this.animRight = new Animation(500, Assets.player_right);
		
		this.lastAttackTimer = 0;
		this.attackCooldown = 800;
		this.attackTimer = attackCooldown;
		
		this.lastImage = this.animDown.getCurrentFrame();
		
		this.inventory = new Inventory(this.handler);
	}

	@Override
	public void update() {
		this.animDown.update();
		this.animUp.update();
		this.animLeft.update();
		this.animRight.update();
		
		this.getInput();
		this.move();
		this.handler.getGameCamera().centerOnEntity(this);
		
		this.checkAttacks();
		
		this.inventory.update();
	}

	private void checkAttacks(){
		
		this.attackTimer += System.currentTimeMillis() - this.lastAttackTimer;
		this.lastAttackTimer  = System.currentTimeMillis();
		if(this.attackTimer < this.attackCooldown){
			return;
		}
		
		if(this.inventory.isActive()){
			return;
		}
		Rectangle collisionBox = this.getCollisionBounds(0, 0);
		Rectangle attackRectangle = new Rectangle();
		int porteAttack = 20;
		attackRectangle.width = porteAttack;
		attackRectangle.height = porteAttack;
		
		if(this.handler.getKeyManager().attackUp){
			attackRectangle.x = collisionBox.x + collisionBox.width/2 - attackRectangle.width/2;
			attackRectangle.y = collisionBox.y - attackRectangle.height;
		}
		else if(this.handler.getKeyManager().attackDown){
			attackRectangle.x = collisionBox.x + collisionBox.width/2 - attackRectangle.width/2;
			attackRectangle.y = collisionBox.y + collisionBox.height;
		}
		else if(this.handler.getKeyManager().attackLeft){
			attackRectangle.x = collisionBox.x - attackRectangle.width;
			attackRectangle.y = collisionBox.y + collisionBox.height/2 - attackRectangle.height/2;
		}
		else if(this.handler.getKeyManager().attackRight){
			attackRectangle.x = collisionBox.x + collisionBox.width;
			attackRectangle.y = collisionBox.y + collisionBox.height/2 - attackRectangle.height/2;
		}
		else{
			return;
		}
		
		this.attackTimer = 0;
		
		for(Entity e : this.handler.getWorld().getEntityManager().getEntities()){
			if(e.equals(this)){
				continue;
			}
			else if (e.getCollisionBounds(0, 0).intersects(attackRectangle)){
				e.hurt(1);
				return;
			}
		}
	}
	
	@Override
	public void render(Graphics g) {
		g.drawImage(this.getCurrentAnimationFrame(), (int)(this.x - this.handler.getGameCamera().getxOffset()), (int)(this.y - this.handler.getGameCamera().getyOffset()), this.width, this.height, null);
		/*
		g.setColor(Color.RED);
		g.fillRect((int) (this.x + this.bounds.x - this.handler.getGameCamera().getxOffset()), (int) (this.y + this.bounds.y - this.handler.getGameCamera().getyOffset()), this.bounds.width, this.bounds.height);
		*/
		this.inventory.render(g);
	}
	
	public void postRender(Graphics g){
		this.inventory.render(g);
	}
	
	private void getInput(){
		this.xMove = 0;
		this.yMove = 0;
		
		if(this.inventory.isActive()){
			return;
		}
		
		if(this.handler.getKeyManager().up){
			this.yMove = -this.speed;
		}
		if (this.handler.getKeyManager().down){
			this.yMove = this.speed;
		}
		if (this.handler.getKeyManager().left){
			this.xMove = -this.speed;
		}
		if (this.handler.getKeyManager().right){
			this.xMove = this.speed;
		}
	}
	
	private BufferedImage getCurrentAnimationFrame(){
		if(this.yMove > 0){
			this.lastImage = this.animDown.getCurrentFrame();
		}
		else if(this.yMove < 0){
			this.lastImage = this.animUp.getCurrentFrame();
		}
		else if(this.xMove < 0){
			this.lastImage = this.animLeft.getCurrentFrame();
		}
		else if(this.xMove > 0){
			this.lastImage = this.animRight.getCurrentFrame();
		}

		return this.lastImage;
		
	}

	@Override
	public void die() {
		// TODO Auto-generated method stub
		
	}

	public Inventory getInventory() {
		return inventory;
	}
}
