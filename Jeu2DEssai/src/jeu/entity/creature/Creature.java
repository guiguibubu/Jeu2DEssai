package jeu.entity.creature;

import jeu.Handler;
import jeu.entity.Entity;
import jeu.tile.Tile;

public abstract class Creature extends Entity {
	
	public static final float DEFAULT_SPEED = 3.0f;
	public static final int DEFAULT_CREATURE_WIDTH = 64,DEFAULT_CREATURE_HEIGHT = 64;
	
	protected float speed;
	protected float xMove, yMove;
		
	public Creature(Handler handler, float x, float y, int width, int height) {
		super(handler, x, y, width, height);
		this.speed = Creature.DEFAULT_SPEED;
		this.xMove = 0;
		this.yMove = 0;
	}

	public void move(){
		if(!this.checkEntityCollision(xMove, 0f))
			this.moveX();
		if(!this.checkEntityCollision(0f, yMove))
			this.moveY();
	}
	
	public void moveX(){
		int tx;
		if(this.x + this.xMove < 0 || this.x + this.xMove + this.width > this.handler.getWorld().getWidth()*Tile.TILEWIDTH){
			this.xMove = 0;
		}
		if(this.xMove > 0){
			tx = (int) (this.x + this.xMove + this.bounds.x + this.bounds.width) / Tile.TILEWIDTH;
			if (!this.collisionWithTil(tx, (int) (this.y + this.bounds.y) / Tile.TILEHEIGHT) && !this.collisionWithTil(tx, (int) (this.y + this.bounds.y + this.bounds.height) / Tile.TILEHEIGHT)){
				this.x += this.xMove;
			}
			else{
				this.x = tx * Tile.TILEWIDTH - this.bounds.x - this.bounds.width -1;
			}
		}
		else if (this.xMove < 0){
			tx = (int) (this.x + this.xMove + this.bounds.x) / Tile.TILEWIDTH;
			if (!this.collisionWithTil(tx, (int) (this.y + this.bounds.y) / Tile.TILEHEIGHT) && !this.collisionWithTil(tx, (int) (this.y + this.bounds.y + this.bounds.height) / Tile.TILEHEIGHT)){
				this.x += this.xMove;
			}
			else{
				this.x = tx * Tile.TILEWIDTH + Tile.TILEWIDTH - this.bounds.x;
			}
		}
	}
	
	public void moveY(){
		int ty;
		if(this.y + this.yMove< 0 || this.y + this.yMove + this.height > this.handler.getWorld().getHeight()*Tile.TILEHEIGHT){
			this.yMove = 0;
		}
		if(this.yMove > 0){
			ty = (int) (this.y + this.yMove + this.bounds.y + this.bounds.height) / Tile.TILEHEIGHT;
			if (!this.collisionWithTil((int) (this.x + this.bounds.x) / Tile.TILEHEIGHT, ty) && !this.collisionWithTil((int) (this.x + this.bounds.x + this.bounds.width) / Tile.TILEHEIGHT, ty)){
				this.y += this.yMove;
			}
			else{
				this.y = ty * Tile.TILEHEIGHT - this.bounds.y - this.bounds.height -1;
			}
		}
		else if (this.yMove < 0){
			ty = (int) (this.y + this.yMove + this.bounds.y) / Tile.TILEHEIGHT;
			if (!this.collisionWithTil((int) (this.x + this.bounds.x) / Tile.TILEHEIGHT, ty) && !this.collisionWithTil((int) (this.x + this.bounds.x + this.bounds.width) / Tile.TILEHEIGHT, ty)){
				this.y += this.yMove;
			}
			else{
				this.y = ty * Tile.TILEHEIGHT + Tile.TILEHEIGHT - this.bounds.y;
			}
		}
	}
	
	protected boolean collisionWithTil(int x, int y){
		return this.handler.getWorld().getTile(x, y).isSolid();
	}
	
	public float getxMove() {
		return xMove;
	}

	public void setxMove(float xMove) {
		this.xMove = xMove;
	}

	public float getyMove() {
		return yMove;
	}

	public void setyMove(float yMove) {
		this.yMove = yMove;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}
}
