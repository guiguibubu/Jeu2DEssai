package jeu.inventory;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import jeu.Handler;
import jeu.gfx.Assets;
import jeu.gfx.Text;
import jeu.items.Item;

public class Inventory {

	private Handler handler;
	private boolean active;
	private ArrayList<Item> inventoryItems;
	
	private int invX, invY, invWidth, invHeight;
	private int invListCenterX, invListCenterY;
	private int invListSpacing;
	
	private int invImageX, invImageY, invImageWidth, invImageHeight;
	private int invCountX, invCountY;
	
	private int selectedItem;
	
	public Inventory(Handler handler) {
		this.handler = handler;
		this.active = false;
		this.inventoryItems = new ArrayList<Item>();
		
		this.invX = 64;
		this.invY = 48;
		this.invWidth = 512;
		this.invHeight = 384;
		this.invListCenterX = this.invX + 171;
		this.invListCenterY = this.invY + this.invHeight/2 + 5;
		this.invListSpacing = 30;
		this.invImageX = 452;
		this.invImageY = 82;
		this.invImageWidth= 64;
		this.invImageHeight= 64;
		this.invCountX= 484;
		this.invCountY= 172;
		
		this.selectedItem = 0;
	}

	public void update(){
		if(this.handler.getKeyManager().keyJustPressed(KeyEvent.VK_I)){
			this.active = !this.active;
		}
		if(this.active && this.handler.getKeyManager().keyJustPressed(KeyEvent.VK_ESCAPE)){
			this.active = false;
		}
		if(!this.active){
			return;
		}
		
		if(this.handler.getKeyManager().keyJustPressed(KeyEvent.VK_DOWN)){
			this.selectedItem--;
		}
		if(this.handler.getKeyManager().keyJustPressed(KeyEvent.VK_UP)){
			this.selectedItem++;
		}
		if(this.selectedItem < 0){
			this.selectedItem = this.inventoryItems.size() - 1; 
		}
		if(this.selectedItem >= this.inventoryItems.size()){
			this.selectedItem = 0;
		}
	}
	
	public void render(Graphics g){
		if(!this.active){
			return;
		}
		
		g.drawImage(Assets.inventoryScreen, this.invX, this.invY, null);
		
		int nbItem = this.inventoryItems.size();
		if(nbItem != 0){
			for(int i = -5; i < 6; i++){
				if(this.selectedItem + i < 0 || this.selectedItem + i >= nbItem){
					continue;
				}
				if(i == 0){
					Text.drawString(g, "> "+this.inventoryItems.get(this.selectedItem + i).getName()+" <", this.invListCenterX , this.invListCenterY + i*this.invListSpacing, true, Color.WHITE, Assets.font28);
				}
				else{
					Text.drawString(g, this.inventoryItems.get(this.selectedItem + i).getName(), this.invListCenterX , this.invListCenterY + i*this.invListSpacing, true, Color.DARK_GRAY, Assets.font28);
				}
			}
			
			Item item = this.inventoryItems.get(this.selectedItem);
			g.drawImage(item.getTexture(), this.invImageX, this.invImageY, this.invImageWidth, this.invImageHeight, null);
			Text.drawString(g, Integer.toString(item.getCount()), this.invCountX, this.invCountY, true, Color.WHITE, Assets.font28);
		}
	}
	
	public void addItem(Item item){
		for(Item i : this.inventoryItems){
			if(i.getId() == item.getId()){
				i.setCount(i.getCount() + item.getCount());
				return;
			}
		}
		this.inventoryItems.add(item);
	}

	public Handler getHandler() {
		return this.handler;
	}

	public void setHandler(Handler handler) {
		this.handler = handler;
	}

	public boolean isActive() {
		return this.active;
	}
	
	
}
