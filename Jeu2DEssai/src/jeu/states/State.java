package jeu.states;

import java.awt.Graphics;

import jeu.Handler;

public abstract class State {
	
	private static State currentState = null;
	
	protected Handler handler;
	
	public State(Handler handler){
		this.handler = handler;
	}
	
	public static void setState(State state){
		State.currentState = state;
	}
	
	public static State getState(){
		return State.currentState;
	}
	public abstract void update();
	
	public abstract void render (Graphics g);
	
}
