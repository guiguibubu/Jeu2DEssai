package jeu.states;

import java.awt.Graphics;

import jeu.Handler;
import jeu.entity.creature.Player;
import jeu.entity.statics.Tree;
import jeu.world.World;

public class GameState extends State {

	private World world;

	
	public GameState(Handler handler){
		super(handler);
		this.world = new World(this.handler, "res/worlds/world4.txt");
		this.handler.setWorld(this.world);
	}
	
	@Override
	public void update() {
		this.world.update();
	}

	@Override
	public void render(Graphics g) {
		this.world.render(g);
	}

}
