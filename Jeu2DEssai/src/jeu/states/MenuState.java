package jeu.states;

import java.awt.Graphics;

import jeu.Handler;
import jeu.gfx.Assets;
import jeu.ui.ClickListener;
import jeu.ui.UIImageButton;
import jeu.ui.UIManager;

public class MenuState extends State {

	private UIManager uiManager;
	
	public MenuState(Handler handler){
		super(handler);
		this.uiManager = new UIManager(this.handler);
		this.handler.getMouseManager().setUIManager(this.uiManager);
		ClickListener clicker = new ClickListener() {
			
			@Override
			public void onClick() {
				handler.getMouseManager().setUIManager(null);
				State.setState(handler.getGame().gameState);
				
			}
		};
		this.uiManager.addObject(new UIImageButton((this.handler.getWidth())/2, (this.handler.getHeight())/2, 128, 64, Assets.boutton_start, clicker));
		
	}
	@Override
	public void update() {
		this.uiManager.update();
	}

	@Override
	public void render(Graphics g) {
		this.uiManager.render(g);

	}

}
