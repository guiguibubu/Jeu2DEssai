package jeu.tile;

import jeu.gfx.Assets;

public class WoodTile extends Tile {

	public WoodTile(int id) {
		super(Assets.tree, id);
	}
	
	@Override
	public boolean isSolid(){
		return true;
	}

}
