package jeu;

import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import jeu.display.Display;
import jeu.gfx.Assets;
import jeu.gfx.GameCamera;
import jeu.input.KeyManager;
import jeu.input.MouseManager;
import jeu.states.GameState;
import jeu.states.MenuState;
import jeu.states.SettingState;
import jeu.states.State;

public class Game implements Runnable{
	
	private Display display; 
	private String title;
	private int width, height;
	
	private boolean running = false;
	private Thread thread;
	
	private BufferStrategy bs;
	private Graphics graphics;
	
	//States
	public State gameState;
	public State menuState;
	public State settingState;
	
	private KeyManager keyManager;
	private MouseManager mouseManager;
	
	private GameCamera gameCamera;
	
	private Handler handler;
	
	public Game(String title, int width, int heigth){
		this.title = title;
		this.width = width;
		this.height = heigth;
		this.keyManager = new KeyManager();
		this.mouseManager = new MouseManager();
	}
	
	public KeyManager getKeyManager(){
		return this.keyManager;
	}
	
	public MouseManager getMouseManager() {
		return this.mouseManager;
	}
	
	public GameCamera getGameCamera(){
		return this.gameCamera;
	}
	
	public int getWidth(){
		return this.width;
	}
	
	public int getHeight(){
		return this.height;
	}
	

	public void run(){
		this.init();
		
		this.running = true;
		
		int fps = 60;
		double tempsEntreUpdates = 1000000000 / fps; // temps en nano-secondes
		double deltaT = 0;
		
		long now;
		long lastTime = System.nanoTime();
		while(this.running){
			now = System.nanoTime();
			deltaT += (now - lastTime);
			lastTime = now;
			if(deltaT >= tempsEntreUpdates){
				this.update();
				this.render();
				//nbUpdates++;
				deltaT = 0;
			}
			if (this.keyManager.quit){
				//this.running = false;
			}
		}
		this.stop();
	}
	
	
	//private BufferedImage testImage;
	//private SpriteSheet sheet;
	//private BufferedImage testMosaique;
	
	private void init(){
		this.display = new Display(this.title, this.width, this.height);
		this.display.getFrame().addKeyListener(this.keyManager);
		this.display.getFrame().addMouseListener(this.mouseManager);
		this.display.getFrame().addMouseMotionListener(this.mouseManager);
		this.display.getCanvas().addMouseListener(this.mouseManager);
		this.display.getCanvas().addMouseMotionListener(this.mouseManager);
		//this.testImage = ImageLoader.loadImage("/textures/test.png");
		//this.testMosaique = ImageLoader.loadImage("/textures/Mosaique.png");
		//this.sheet = new SpriteSheet(this.testMosaique);
		Assets.init();
		
		this.handler = new Handler(this);
		
		this.gameCamera = new GameCamera(this.handler, 0, 0);
		this.gameState = new GameState(this.handler);
		this.menuState = new MenuState(this.handler);
		this.settingState = new SettingState(this.handler);
		State.setState(this.menuState);
	}
	
	private void update(){
		this.keyManager.update();
		
		if(State.getState() != null){
			State.getState().update();
		}
	}
	
	private void render(){
		this.bs = this.display.getCanvas().getBufferStrategy();
		if(this.bs == null){
			this.display.getCanvas().createBufferStrategy(3);
			return;
		}
		this.graphics = this.bs.getDrawGraphics();
		//Clear écran
		this.graphics.clearRect(0, 0, this.width, this.height);
		//Début Dessin
		if(State.getState() != null){
			State.getState().render(this.graphics);
		}
		/*
		this.graphics.drawImage(sheet.crop(175, 0, 523-175, 242), 10, 10, null);
		this.graphics.drawImage(sheet.crop(352, 245, 520-352, 482-245), 500, 200, null);
		*/
		/*
		this.graphics.drawImage(this.testImage, 40, 20, null);
		*/
		/*
		this.graphics.setColor(Color.BLUE);
		this.graphics.drawRect(10, 50, 100, 300);
		
		this.graphics.setColor(Color.blue);
		this.graphics.fillRect(110, 50, 100, 300);
		*/
		//Fin Dessin
		
		this.bs.show();
		this.graphics.dispose();
	}
	
	public synchronized void start(){
		if(this.running)
			return;
		this.running = true;
		this.thread = new Thread(this);
		this.thread.start();//va chercher le run() car Runnable
	}
	
	public synchronized void stop(){
		if(!this.running){
			return;
		}
		this.running = false;
		try {
			this.display.getFrame().setVisible(false);
			this.thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
