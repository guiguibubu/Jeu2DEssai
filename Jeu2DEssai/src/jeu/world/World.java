package jeu.world;

import java.awt.Graphics;

import jeu.Handler;
import jeu.entity.EntityManager;
import jeu.entity.creature.Player;
import jeu.entity.statics.Rock;
import jeu.entity.statics.Tree;
import jeu.items.ItemManager;
import jeu.tile.Tile;
import jeu.utils.Utils;

public class World {

	private Handler handler;
	private int width, height;
	private int spawnX, spawnY;
	private int[][] tiles;
	
	private EntityManager entityManager;
	
	private ItemManager itemManager;

	public World(Handler handler, String path) {
		this.handler = handler;
		this.entityManager = new EntityManager(this.handler, new Player(this.handler, 100, 100));
		this.itemManager = new ItemManager(this.handler);
		this.loadWorld(path);
		
		this.entityManager.getPlayer().setX(this.spawnX);
		this.entityManager.getPlayer().setY(this.spawnY);
		
		this.entityManager.addEntity(new Tree(this.handler, 100, 200));
		
		this.entityManager.addEntity(new Rock(this.handler, 100, 400));
	}
	
	public void update(){
		this.entityManager.update();
		this.itemManager.update();
	}
	
	public void render(Graphics g){
		int xStart = (int)Math.max(0, this.handler.getGameCamera().getxOffset() / Tile.TILEWIDTH);
		int xEnd = (int)Math.min(this.width-1, (this.handler.getGameCamera().getxOffset() + this.handler.getWidth()) / Tile.TILEWIDTH) + 1;
		int yStart = (int)Math.max(0, this.handler.getGameCamera().getyOffset() / Tile.TILEHEIGHT);
		int yEnd = (int)Math.min(this.height-1, (this.handler.getGameCamera().getyOffset() + this.handler.getHeight()) / Tile.TILEHEIGHT) + 1;
		
		for(int y = yStart; y < yEnd; y++){
			for(int x = xStart; x < xEnd; x++){
				this.getTile(x, y).render(g,(int) (x*Tile.TILEWIDTH - this.handler.getGameCamera().getxOffset()), (int) (y*Tile.TILEHEIGHT - this.handler.getGameCamera().getyOffset()));
			}
		}
		this.itemManager.render(g);
		this.entityManager.render(g);
	}
	
	public Tile getTile(int x, int y){
		if(x < 0 || y < 0 || x >= this.width || y >= this.height){
			return Tile.grassTile;
		}
		
		Tile t = Tile.tiles[this.tiles[x][y]];
		if(t == null){
			return Tile.dirtTile;
		}
		return t;
	}
	
	private void loadWorld(String path){
		String file = Utils.loadFileAsString(path);
		String[] tokens = file.split("\\s+");

		this.width = Utils.parseInt(tokens[0]);
		this.height = Utils.parseInt(tokens[1]);
		this.spawnX = Utils.parseInt(tokens[2]);
		this.spawnY = Utils.parseInt(tokens[3]);
		
		this.tiles = new int[this.width][this.height];
		for(int y = 0; y < this.height; y++){
			for(int x = 0; x < this.width; x++){
				this.tiles[x][y] = Utils.parseInt(tokens[(x+y*this.width)+4]);
			}
		}
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public Handler getHandler() {
		return handler;
	}

	public void setHandler(Handler handler) {
		this.handler = handler;
	}

	public ItemManager getItemManager() {
		return itemManager;
	}

	public void setItemManager(ItemManager itemManager) {
		this.itemManager = itemManager;
	}
}
