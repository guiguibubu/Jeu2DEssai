package jeu.display;

import java.awt.Canvas;
import java.awt.Dimension;

import javax.swing.JFrame;

import jeu.gfx.ImageLoader;
import jeu.gfx.SpriteSheet;

public class Display {
	
	private JFrame frame;
	private Canvas canvas;
	
	private String title;
	private int width, heigth;
	
	public Display(String title, int width, int heigth){
		this.title = title;
		this.width = width;
		this.heigth = heigth;
		
		this.createDisplay();
	}
	
	private void createDisplay(){
		this.frame = new JFrame(this.title);
		this.frame.setSize(this.width, this.heigth);
		this.frame.setIconImage(new SpriteSheet(ImageLoader.loadImage("/textures/Mosaique.png")).crop(352, 245, 520-352, 482-245));
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setResizable(false);
		this.frame.setLocationRelativeTo(null);
		this.frame.setVisible(true);
		
		this.canvas = new Canvas();
		Dimension dimension = new Dimension(this.width, this.heigth);
		this.canvas.setPreferredSize(dimension);
		this.canvas.setMaximumSize(dimension);
		this.canvas.setMinimumSize(dimension);
		this.canvas.setFocusable(false);
		
		this.frame.add(this.canvas);
		this.frame.pack();
	}
	
	public Canvas getCanvas(){
		return this.canvas;
	}
	
	public JFrame getFrame(){
		return this.frame;
	}
}
