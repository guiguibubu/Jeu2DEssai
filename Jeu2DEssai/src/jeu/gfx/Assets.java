package jeu.gfx;

import java.awt.Font;
import java.awt.image.BufferedImage;

public class Assets {
	
	private static final int width = 32, height = 32;
	
	public static Font font28, font48;
	
	public static BufferedImage dirt, grass, stone,tree,wall;
	public static BufferedImage wood;
	public static BufferedImage[] player_down;
	public static BufferedImage[] player_up;
	public static BufferedImage[] player_left;
	public static BufferedImage[] player_right;
	
	public static BufferedImage[] zombie_down;
	public static BufferedImage[] zombie_up;
	public static BufferedImage[] zombie_left;
	public static BufferedImage[] zombie_right;
	
	public static BufferedImage[] boutton_start;
	
	public static BufferedImage inventoryScreen;
	
	public static void init(){
		
		Assets.font28 = FontLoader.loadFont("res/font/slkscr.ttf", 28);
		Assets.font48 = FontLoader.loadFont("res/font/slkscr.ttf", 48);
		
		SpriteSheet sheet = new SpriteSheet(ImageLoader.loadImage("/textures/sheetTuto.png"));
		
		Assets.tree = sheet.crop(0, 0, Assets.width, 2*Assets.height);
		Assets.dirt = sheet.crop(Assets.width, 0, Assets.width, Assets.height);
		Assets.grass = sheet.crop(2*Assets.width, 0, Assets.width, Assets.height);
		Assets.wall = sheet.crop(3*Assets.width, 0, Assets.width, Assets.height);
		
		Assets.wood = sheet.crop(Assets.width, Assets.height, Assets.width, Assets.height);
		Assets.stone = sheet.crop(0, 2*Assets.height, Assets.width, Assets.height);
		
		Assets.inventoryScreen = ImageLoader.loadImage("/textures/inventoryScreen.png");
		
		Assets.player_down = new BufferedImage[2];
		Assets.player_up = new BufferedImage[2];
		Assets.player_left = new BufferedImage[2];
		Assets.player_right = new BufferedImage[2];
		
		Assets.player_down[0] = sheet.crop(4*Assets.width, 0, Assets.width, Assets.height);
		Assets.player_down[1] = sheet.crop(5*Assets.width, 0, Assets.width, Assets.height);
		
		Assets.player_up[0] = sheet.crop(6*Assets.width, 0, Assets.width, Assets.height);
		Assets.player_up[1] = sheet.crop(7*Assets.width, 0, Assets.width, Assets.height);
		
		Assets.player_right[0] = sheet.crop(4*Assets.width, Assets.height, Assets.width, Assets.height);
		Assets.player_right[1] = sheet.crop(5*Assets.width, Assets.height, Assets.width, Assets.height);
		
		Assets.player_left[0] = sheet.crop(6*Assets.width, Assets.height, Assets.width, Assets.height);
		Assets.player_left[1] = sheet.crop(7*Assets.width, Assets.height, Assets.width, Assets.height);
		
		Assets.zombie_down = new BufferedImage[2];
		Assets.zombie_up = new BufferedImage[2];
		Assets.zombie_left = new BufferedImage[2];
		Assets.zombie_right = new BufferedImage[2];
		
		Assets.zombie_down[0] = sheet.crop(4*Assets.width, 2*Assets.height, Assets.width, Assets.height);
		Assets.zombie_down[1] = sheet.crop(5*Assets.width, 2*Assets.height, Assets.width, Assets.height);
		
		Assets.zombie_up[0] = sheet.crop(6*Assets.width, 2*Assets.height, Assets.width, Assets.height);
		Assets.zombie_up[1] = sheet.crop(7*Assets.width, 2*Assets.height, Assets.width, Assets.height);
		
		Assets.zombie_right[0] = sheet.crop(4*Assets.width, 3*Assets.height, Assets.width, Assets.height);
		Assets.zombie_right[1] = sheet.crop(5*Assets.width, 3*Assets.height, Assets.width, Assets.height);
		
		Assets.zombie_left[0] = sheet.crop(6*Assets.width, 3*Assets.height, Assets.width, Assets.height);
		Assets.zombie_left[1] = sheet.crop(7*Assets.width, 3*Assets.height, Assets.width, Assets.height);
		
		Assets.boutton_start = new BufferedImage[2];
		
		Assets.boutton_start[0] = sheet.crop(6*Assets.width, 4*Assets.height, 2*Assets.width, Assets.height);
		Assets.boutton_start[1] = sheet.crop(6*Assets.width, 5*Assets.height, 2*Assets.width, Assets.height);
	}
	
}
