package jeu.gfx;

import java.awt.image.BufferedImage;

public class Animation {

	private int speed, index;
	private BufferedImage[] frames;
	
	private long lastTime;
	private long timer;
	
	public Animation (int speed, BufferedImage[] frames){
		this.speed = speed;
		this.frames = frames;
		this.index = 0;
		this.lastTime = System.currentTimeMillis();
		this.timer = 0;
	}
	
	public void update(){
		this.timer +=  System.currentTimeMillis() - this.lastTime;
		this.lastTime =  System.currentTimeMillis();
		if(this.timer > this.speed){
			this.index = (this.index+1)%(this.frames.length) ;
			this.timer = 0;
		}
	}
	
	public BufferedImage getCurrentFrame(){
		return this.frames[index];
	}
}
