package jeu.gfx;

import jeu.Handler;
import jeu.entity.Entity;
import jeu.tile.Tile;

public class GameCamera {
	
	private float xOffset, yOffset;
	private Handler handler;
	
	public GameCamera(Handler handler, float xOffset, float yOffset) {
		this.xOffset = xOffset;
		this.yOffset = yOffset;
		this.handler = handler;
	}
	
	public void checkBlackSpace(){
		if(this.xOffset < 0){
			this.xOffset = 0;
		}
		else if(this.xOffset > this.handler.getWorld().getWidth() * Tile.TILEWIDTH - this.handler.getWidth()){
			this.xOffset = this.handler.getWorld().getWidth() * Tile.TILEWIDTH - this.handler.getWidth();
		}
		
		if(this.yOffset < 0){
			this.yOffset = 0;
		}
		else if(this.yOffset > this.handler.getWorld().getHeight() * Tile.TILEHEIGHT - this.handler.getHeight()){
			this.yOffset = this.handler.getWorld().getHeight() * Tile.TILEHEIGHT - this.handler.getHeight();
		}
	}
	
	public void centerOnEntity(Entity e){
		this.xOffset = e.getX() - this.handler.getWidth()/2 + e.getWidth()/2;
		this.yOffset = e.getY() - this.handler.getHeight()/2 + e.getHeight()/2;
		this.checkBlackSpace();
	}
	
	public void move(float xAmt, float yAmt){
		this.xOffset += xAmt;
		this.yOffset += yAmt;
		this.checkBlackSpace();
	}
	
	public float getxOffset() {
		return xOffset;
	}

	public void setxOffset(float xOffset) {
		this.xOffset = xOffset;
	}

	public float getyOffset() {
		return yOffset;
	}

	public void setyOffset(float yOffset) {
		this.yOffset = yOffset;
	}

}
