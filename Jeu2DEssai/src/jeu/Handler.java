package jeu;

import jeu.gfx.GameCamera;
import jeu.input.KeyManager;
import jeu.input.MouseManager;
import jeu.world.World;

public class Handler {
	
	private Game game;
	private World world;
	
	public Handler(Game game){
		this.game = game;
	}
	
	public int getWidth(){
		return this.game.getWidth();
	}
	
	public int getHeight(){
		return this.game.getHeight();
	}
	
	public KeyManager getKeyManager(){
		return this.game.getKeyManager();
	}
	
	public MouseManager getMouseManager(){
		return this.game.getMouseManager();
	}
	
	public GameCamera getGameCamera(){
		return this.game.getGameCamera();
	}
	
	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public World getWorld() {
		return world;
	}

	public void setWorld(World world) {
		this.world = world;
	}
	
	
}
