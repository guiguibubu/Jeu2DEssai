package jeu.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyManager implements KeyListener {
	
	private boolean[] keys, justPressed, cantPress;
	public boolean up, down, left, right;
	public boolean quit;
	public boolean attackUp, attackDown, attackLeft, attackRight;
	
	public KeyManager() {
		this.keys = new boolean[256];
		this.justPressed = new boolean[this.keys.length];
		this.cantPress = new boolean[this.keys.length];
	}
	
	public void update(){
		for(int i = 0; i< this.keys.length; i++){
			if(this.cantPress[i] && !this.keys[i]){
				this.cantPress[i] = false;
			}
			else if (this.justPressed[i]){
				this.cantPress[i] = true;
				this.justPressed[i] = false;
			}
			if(!this.cantPress[i] && this.keys[i]){
				this.justPressed[i] = true;
			}
		}
		
		this.up = this.keys[KeyEvent.VK_Z];
		this.down = this.keys[KeyEvent.VK_S];
		this.left = this.keys[KeyEvent.VK_Q];
		this.right = this.keys[KeyEvent.VK_D];
		
		this.attackUp = this.keys[KeyEvent.VK_UP];
		this.attackDown= this.keys[KeyEvent.VK_DOWN];
		this.attackLeft = this.keys[KeyEvent.VK_LEFT];
		this.attackRight = this.keys[KeyEvent.VK_RIGHT];
		
		this.quit = this.keys[KeyEvent.VK_ESCAPE];
	}
	
	public boolean keyJustPressed(int keyCode){
		  if(keyCode < 0 || keyCode >= this.keys.length)
		    return false;
		  return this.justPressed[keyCode];
		}
	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() < 0 || e.getKeyCode() >= this.keys.length){
			return;
		}
		this.keys[e.getKeyCode()] = true;
	}

	@Override
	public void keyReleased(KeyEvent e) {
		this.keys[e.getKeyCode()] = false;
	}

	@Override
	public void keyTyped(KeyEvent arg0) {

	}

}
