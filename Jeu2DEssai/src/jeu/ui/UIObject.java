package jeu.ui;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;

public abstract class UIObject implements ClickListener{

	protected float x,y;
	protected int width, height;
	protected Rectangle bounds;
	protected boolean hovering;
	
	public UIObject(float x, float y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.bounds = new Rectangle((int) this.x, (int) this.y, this.width, this.height);
		this.hovering = false;
	}
	
	public abstract void update();
	public abstract void render(Graphics g);
	public abstract void onClick();
	
	public void onMouseMove(MouseEvent e){
		if(this.bounds.contains(e.getX(), e.getY())){
			this.hovering = true;
		}
		else{
			this.hovering = false;
		}
	}
	
	public void onMouseReleased(MouseEvent e){
		if(this.hovering){
			this.onClick();
		}
	}
	
	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public boolean isHovering() {
		return hovering;
	}

	public void setHovering(boolean hovering) {
		this.hovering = hovering;
	}
}
