package jeu.ui;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import jeu.Handler;

public class UIManager {
	
	private Handler handler;
	private ArrayList<UIObject> objects;
	
	public UIManager(Handler handler) {
		this.handler = handler;
		this.objects = new ArrayList<UIObject>();
	}
	
	public void update(){
		for(UIObject o : this.objects){
			o.update();
		}
	}
	
	public void render(Graphics g){
		for(UIObject o : this.objects){
			o.render(g);
		}
	}
	
	public void onMouseMove(MouseEvent e){
		for(UIObject o : this.objects){
			o.onMouseMove(e);
		}
	}
	
	public void onMouseRelease(MouseEvent e){
		for(UIObject o : this.objects){
			o.onMouseReleased(e);
		}
	}
	
	public void addObject(UIObject o){
		this.objects.add(o);
	}
	
	public void removeObject(UIObject o){
		this.objects.remove(o);
	}

	public Handler getHandler() {
		return this.handler;
	}

	public void setHandler(Handler handler) {
		this.handler = handler;
	}

}
