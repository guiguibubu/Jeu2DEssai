package jeu.items;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Iterator;

import jeu.Handler;

public class ItemManager {

	private Handler handler;
	private ArrayList<Item> items;
	
	public ItemManager(Handler handler) {
		this.handler = handler;
		this.items = new ArrayList<Item>();
	}

	public void update(){
		Iterator<Item> it = this.items.iterator();
		while(it.hasNext()){
			Item item = it.next();
			item.update();
			if(item.isPickedUp()){
				it.remove();
			}
		}
	}
	
	public void render(Graphics g){
		for(Item item : this.items){
			item.render(g);
		}
	}
	
	public void addItem(Item item){
		item.setHandler(this.handler);
		this.items.add(item);
	}
	
	public Handler getHandler(){
		return this.handler;
	}
}
