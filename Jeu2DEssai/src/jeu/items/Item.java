package jeu.items;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import jeu.Handler;
import jeu.gfx.Assets;

public class Item {

	
	public static Item[] items = new Item[256];
	public static Item woodItem = new Item(Assets.wood, "Wood", 0);
	public static Item rockItem = new Item(Assets.stone, "Stone", 1);
	
	public static final int ITEM_WIDTH = 32, ITME_HEIGHT = 32, PICKED_UP = -1;
	
	protected Handler handler;
	protected BufferedImage texture;
	protected String name;
	protected final int id;
	
	protected int x, y, count;
	protected boolean pickedUp;
	protected Rectangle bounds;
	
	public Item(BufferedImage texture, String name, int id) {
		this.texture = texture;
		this.name = name;
		this.id = id;
		this.count = 1;
		
		this.bounds = new Rectangle(this.x, this.y, Item.ITEM_WIDTH, Item.ITME_HEIGHT);
		
		this.pickedUp = false;
		
		Item.items[id] = this;
	}
	
	public void update(){
		if(this.handler.getWorld().getEntityManager().getPlayer().getCollisionBounds(0, 0).intersects(this.bounds)){
			this.pickedUp = true;
			this.handler.getWorld().getEntityManager().getPlayer().getInventory().addItem(this);
		}
	}
	
	public void render(Graphics g){
		if(this.handler == null){
			System.out.println("handler item null");
			return;
		}
		this.render(g, (int) (this.x - this.handler.getGameCamera().getxOffset()), (int) (this.y - this.handler.getGameCamera().getyOffset()));
	}
	
	public void render(Graphics g, int x, int y){
		g.drawImage(this.texture, x, y, Item.ITEM_WIDTH, Item.ITME_HEIGHT, null);
	}

	public Item createNew(int x, int y){
		Item item = new Item(this.texture, this.name, this.id);
		item.setPosition(x, y);
		return item;
	}
	
	public Item createNew(int count){
		Item item = new Item(this.texture, this.name, this.id);
		item.setPickedUp(true);
		item.setCount(count);
		return item;
	}
	
	public void setPosition(int x, int y){
		this.x = x;
		this.y = y;
		this.bounds.x = x;
		this.bounds.y = y;
	}
	
	public boolean isPickedUp(){
		return this.pickedUp;
	}
	
	
	
	public void setPickedUp(boolean pickedUp) {
		this.pickedUp = pickedUp;
	}

	public Handler getHandler() {
		return handler;
	}

	public void setHandler(Handler handler) {
		this.handler = handler;
	}

	public BufferedImage getTexture() {
		return texture;
	}

	public void setTexture(BufferedImage texture) {
		this.texture = texture;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getId() {
		return id;
	}
}
